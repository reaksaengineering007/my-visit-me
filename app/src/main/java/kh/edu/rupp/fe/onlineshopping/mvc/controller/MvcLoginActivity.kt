package kh.edu.rupp.fe.onlineshopping.mvc.controller

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kh.edu.rupp.fe.onlineshopping.MainActivity
import kh.edu.rupp.fe.onlineshopping.R
import kh.edu.rupp.fe.onlineshopping.mvc.model.User

class MvcLoginActivity : AppCompatActivity() {
    private lateinit var user: User
    private lateinit var txtUsername: EditText
    private lateinit var txtPassword: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        user = User()
        txtUsername = findViewById(R.id.editTextTextEmailAddress)
        txtPassword = findViewById(R.id.editTextPassword)
        val buttonLogin = findViewById<Button>(R.id.buttonLogin)
        buttonLogin.setOnClickListener{
            user.setUsername(txtUsername.text.toString())
            user.setPassword(txtPassword.text.toString())
            login(user)
        }
    }
    private fun login(user: User){
        if(user.getUsername() == "admin" && user.getPassword() == "123"){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent);
            finish()
        }else{
            Toast.makeText(this, "Login Failed!", Toast.LENGTH_SHORT).show()
        }
    }
}

package kh.edu.rupp.fe.onlineshopping.mvp.presenter

import kh.edu.rupp.fe.onlineshopping.mvp.model.User
import kh.edu.rupp.fe.onlineshopping.mvp.view.LoginView

class LoginPresenter(
    private var user: User,
    private var loginView: LoginView
) {
    fun login(){
        val username: String = loginView.getUsername()
        val password: String = loginView.getPassword()
        val isSuccessful: Boolean = user.login(username, password)
        loginView.loginResult(isSuccessful)
    }
}
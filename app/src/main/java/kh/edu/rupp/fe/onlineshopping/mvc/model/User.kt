package kh.edu.rupp.fe.onlineshopping.mvc.model

class User {
    private lateinit var username: String
    private lateinit var password: String
    fun getUsername(): String {
        return username
    }
    fun setUsername(username: String) {
        this.username = username
    }
    fun getPassword(): String {
        return password
    }
    fun setPassword(password: String) {
        this.password = password
    }
}
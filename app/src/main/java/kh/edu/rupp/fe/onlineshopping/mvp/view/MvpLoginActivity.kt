package kh.edu.rupp.fe.onlineshopping.mvp.view

import android.content.Intent
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kh.edu.rupp.fe.onlineshopping.MainActivity
import kh.edu.rupp.fe.onlineshopping.R
import kh.edu.rupp.fe.onlineshopping.mvp.model.User
import kh.edu.rupp.fe.onlineshopping.mvp.presenter.LoginPresenter
import kotlin.math.log

class MvpLoginActivity() : AppCompatActivity(), LoginView {
    private lateinit var user: User
    private lateinit var presenter: LoginPresenter
    private lateinit var txtUsername: EditText
    private lateinit var txtPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        user = User()
        txtUsername = findViewById(R.id.editTextTextEmailAddress)
        txtPassword = findViewById(R.id.editTextPassword)
        val buttonLogin = findViewById<Button>(R.id.buttonLogin)
        presenter = LoginPresenter(user, this)
        buttonLogin.setOnClickListener{
            presenter.login()
        }
    }

    override fun getUsername(): String {
        return txtUsername.text.toString()
    }

    override fun getPassword(): String {
        return txtPassword.text.toString()
    }

    override fun loginResult(login: Boolean) {
        if(login){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent);
            finish()
        }
        else{
            Toast.makeText(this, "Login Failed!", Toast.LENGTH_SHORT).show()
        }
    }
}
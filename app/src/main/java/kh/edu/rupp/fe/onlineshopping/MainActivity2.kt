package kh.edu.rupp.fe.onlineshopping

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kh.edu.rupp.fe.onlineshopping.databinding.ActivityMainBinding
import kh.edu.rupp.fe.onlineshopping.fragments.AccountFragment
import kh.edu.rupp.fe.onlineshopping.fragments.CourseFragment
import kh.edu.rupp.fe.onlineshopping.fragments.HomeFragment
import kh.edu.rupp.fe.onlineshopping.fragments.MoreFragment
import kh.edu.rupp.fe.onlineshopping.fragments.ProductFragment
import kh.edu.rupp.fe.onlineshopping.R

public class MainActivity2 : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        navigateFragment(HomeFragment())
        binding.mainBottomNav!!.setOnItemSelectedListener { item ->
            if (item.itemId === R.id.navHome) {
                navigateFragment(HomeFragment())
            } else if (item.itemId === R.id.navCourses) {
                navigateFragment(CourseFragment())
            } else if (item.itemId === R.id.navChat) {
                navigateFragment(ProductFragment())
            } else if (item.itemId === R.id.navAccount) {
                navigateFragment(AccountFragment())
            } else if (item.itemId === R.id.navMore) {
                navigateFragment(MoreFragment())
            } else {
            }
            false
        }
    }
    private fun navigateFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(binding.mainLayout!!.id, fragment)
        fragmentTransaction.commit()
    }
}
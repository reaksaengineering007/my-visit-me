package kh.edu.rupp.fe.onlineshopping;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;

import kh.edu.rupp.fe.onlineshopping.mvc.controller.MvcLoginActivity;
import kh.edu.rupp.fe.onlineshopping.mvp.view.MvpLoginActivity;

public class WelcomeActivity extends AppCompatActivity {
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(WelcomeActivity.this, MvpLoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }
}

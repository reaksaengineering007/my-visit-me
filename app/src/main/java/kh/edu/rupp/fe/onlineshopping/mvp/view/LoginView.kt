package kh.edu.rupp.fe.onlineshopping.mvp.view

interface LoginView {
    fun getUsername(): String
    fun getPassword(): String
    fun loginResult(login: Boolean)
}
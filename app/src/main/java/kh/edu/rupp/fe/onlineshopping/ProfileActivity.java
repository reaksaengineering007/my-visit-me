package kh.edu.rupp.fe.onlineshopping;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import kh.edu.rupp.fe.onlineshopping.databinding.ActivityProfileBinding;

public class ProfileActivity extends AppCompatActivity {
    private ActivityProfileBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Intent intent = getIntent();
        intent.getStringExtra("id");
        // Toast.makeText(this, intent.getStringExtra("message"), Toast.LENGTH_SHORT);
        binding.imgBack.setOnClickListener(v->finish());
    }
}
